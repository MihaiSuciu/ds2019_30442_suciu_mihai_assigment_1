﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database.Models
{
    public enum Gender
    {
        M, F
    }

    public class Caregiver
    {
        public int CaregiverID { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public string Address { get; set; }
        public ICollection<Patient> Patients { get; set; }
    }
}