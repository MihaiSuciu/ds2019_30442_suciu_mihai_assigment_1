﻿using Database.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Database.DAL
{
    public class DataContext : DbContext
    {
        public DataContext() : base("name=Hospital")
        { }
        public virtual DbSet<Caregiver> Caregivers { get; set; }
        public virtual DbSet<Patient> Patients { get; set; }
        public virtual DbSet<MedicationPlan> MedicationPlans { get; set; }
        public virtual DbSet<Medication> Medications { get; set; }
    }
}