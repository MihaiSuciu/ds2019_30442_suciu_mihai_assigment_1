﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { CaregiverComponent } from './caregiver.component';

let component: CaregiverComponent;
let fixture: ComponentFixture<CaregiverComponent>;

describe('caregiver component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ CaregiverComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(CaregiverComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});