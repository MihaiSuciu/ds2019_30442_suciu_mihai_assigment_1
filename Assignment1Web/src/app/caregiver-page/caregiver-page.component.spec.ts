import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverPageComponent } from './caregiver-page.component';

describe('CaregiverPageComponent', () => {
  let component: CaregiverPageComponent;
  let fixture: ComponentFixture<CaregiverPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaregiverPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
