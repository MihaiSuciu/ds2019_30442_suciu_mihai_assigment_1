export class Patient {
    PatientID:number;
    Name:string;
    BirthDate:Date;
    Gender:number;
    Address:string;
}
