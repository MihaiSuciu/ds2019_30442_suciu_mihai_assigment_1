import { Component, OnInit } from '@angular/core';
import { CaregiverService } from './shared/caregiver.service'

@Component({
  selector: 'app-caregivers',
  templateUrl: './caregivers.component.html',
  styleUrls: ['./caregivers.component.css']
})
export class CaregiversComponent implements OnInit {

  constructor(private caregiverService:CaregiverService) { }

  ngOnInit() {
  }

}
