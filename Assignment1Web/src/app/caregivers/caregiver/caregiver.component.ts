import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'

import { CaregiverService } from '../shared/caregiver.service';
import { ToastrService } from 'ngx-toastr'
@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {

  constructor(private caregiverService:CaregiverService,private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){
    if(form!=null){
      form.reset();
    }
    this.caregiverService.selectedCaregiver={
      CaregiverID:0,
      Name:'',
      BirthDate:null,
      Gender:null,
      Address:''
    }
  }
  onSubmit(form:NgForm){
    if(!form.value.CaregiverID){
      this.caregiverService.postCaregiver(form.value)
        .subscribe(data=>{
          this.resetForm(form);
          this.caregiverService.getCaregiverList();
          this.toastr.success('New record Added Successfully!','Caregiver Register');
        })
    }
    else{
      this.caregiverService.putCaregiver(form.value.CaregiverID,form.value)
        .subscribe(data=>{
          this.resetForm(form);
          this.caregiverService.getCaregiverList();
          this.toastr.info("Record Updated Successfully!","Caregiver Register");
        })
    }
  }
}
