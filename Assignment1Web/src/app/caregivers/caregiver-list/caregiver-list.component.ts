import { Component, OnInit } from '@angular/core';
import { CaregiverService } from '../shared/caregiver.service';
import {Caregiver} from '../shared/caregiver.model';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-caregiver-list',
  templateUrl: './caregiver-list.component.html',
  styleUrls: ['./caregiver-list.component.css']
})
export class CaregiverListComponent implements OnInit {

  constructor(private caregiverService:CaregiverService,private toatrs:ToastrService) { }

  ngOnInit() {
    this.caregiverService.getCaregiverList();
  }
  showForEdit(caregiver:Caregiver){
    this.caregiverService.selectedCaregiver=Object.assign({}, caregiver);
    this.caregiverService.selectedCaregiver.BirthDate = new Date(this.caregiverService.selectedCaregiver.BirthDate)
  }
  onDelete(id:number){
    if(confirm('Are you sure to delete this record?')== true){
      this.caregiverService.deleteCaregiver(id)
        .subscribe(x=>{
          this.caregiverService.getCaregiverList();
          this.toatrs.warning("Delete successfully","Caregiver Register")
        })
    }
  }

}
