import { Component, OnInit } from '@angular/core';
import { MedicationService } from '../shared/medication.service';
import {Medication} from '../shared/medication.model';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-medication-list',
  templateUrl: './medication-list.component.html',
  styleUrls: ['./medication-list.component.css']
})
export class MedicationListComponent implements OnInit {

  constructor(private medicationService:MedicationService,private toatrs:ToastrService) { }

  ngOnInit() {
    this.medicationService.getMedicationList();
  }
  showForEdit(medication:Medication){
    this.medicationService.selectedMedication=Object.assign({}, medication);
  }
  onDelete(id:number){
    if(confirm('Are you sure to delete this record?')== true){
      this.medicationService.deleteMedication(id)
        .subscribe(x=>{
          this.medicationService.getMedicationList();
          this.toatrs.warning("Delete successfully","Medication Register")
        })
    }
  }

}
